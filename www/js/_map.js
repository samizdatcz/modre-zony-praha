(function() {
  var map = L.map('map').setView([50.0835494, 14.4341414], 13);
  L.esri.basemapLayer("Gray").addTo(map);
  map.scrollWheelZoom.disable();

  var searchControl = L.esri.Geocoding.geosearch({expanded: true, collapseAfterResult: false, zoomToResult: false}).addTo(map);

  searchControl.on('results', function(data){ 
    if (data.results.length > 0) {
      var popup = L.popup()
        .setLatLng(data.results[0].latlng)
        .setContent(data.results[0].text)
        .openOn(map);
      map.setView(data.results[0].latlng)
    }
  })

   L.TopoJSON = L.GeoJSON.extend({  
     addData: function(jsonData) {    
       if (jsonData.type === "Topology") {
         for (key in jsonData.objects) {
           geojson = topojson.feature(jsonData, jsonData.objects[key]);
           L.GeoJSON.prototype.addData.call(this, geojson);
         }
       }    
       else {
         L.GeoJSON.prototype.addData.call(this, jsonData);
       }
     }  
   });
   // Copyright (c) 2013 Ryan Clark

   var insertPopup = function (latlng, text) {
     var popup = L.popup()
         .setLatLng(latlng)
         .setContent(text)
         .openOn(map);
   };

   var mesice = {
     1: 'ledna',
     2: 'února',
     3: 'března',
     4: 'dubna',
     5: 'května',
     6: 'června',
     7: 'července',
     8: 'srpna',
     9: 'září',
     10: 'října',
     11: 'listopadu'
   };

  var legend = L.control({position: 'topleft'});
  legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'mntselector');
      div.innerHTML = '<select>'
        + '<option value="1">Leden </option>'
        + '<option value="2">Únor</option>'
        + '<option value="3">Březen</option>'
        + '<option value="4">Duben</option>'
        + '<option value="5">Květen</option>'
        + '<option value="6">Červen</option>'
        + '<option value="7">Červenec</option>'
        + '<option value="8">Srpen</option>'
        + '<option value="9">Září</option>'
        + '<option value="10">Říjen</option>'
        + '<option value="11">Listopad</option>'
        + '</select>';
      div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
      return div;
  };
  legend.addTo(map);

  $('.mntselector').on('change', function(e) {
     drawMap(e.target.value);
   })

   var modreZony = new L.TopoJSON();

   function addMzData(mzData){  
     modreZony.addData(mzData);
     modreZony.eachLayer(function (layer) {
         layer.setStyle({
         fillColor : '#a6bddb',
         fillOpacity: 0.6,
         color: 'white',
         weight: 0.5,
         opacity: 0
       });
     });
     modreZony.addTo(map);
   };

   $.getJSON('./data/zony.topo.json')
       .done(addMzData);

   var topoLayer = new L.TopoJSON();

   var drawMap = function(month) {
     topoLayer.clearLayers();

     $.getJSON('./data/binned.topo.json')
       .done(addTopoData);

     function addTopoData(topoData) {
       var vals = [];
       topoData.objects.binned.geometries.forEach(function (el) {
         if (el.properties['cnt_' + month] > 0) {
           vals.push(el.properties['cnt_' + month]);
         }
       });

       var brk = new geostats(vals).getClassQuantile(5);
       var breaks = brk.map(function (x) {
         return parseInt(x)
       });

       var colorScale = chroma.scale(['#fee5d9', '#fcae91', '#fb6a4a', '#de2d26', '#a50f15']).domain(breaks);

       topoLayer.addData(topoData);

       topoLayer.eachLayer(function (layer) {
         if (layer.feature.properties['cnt_' + month] > 0) {
           var fillColor = colorScale(layer.feature.properties['cnt_' + month]).hex();
             layer.setStyle({
             fillColor : fillColor,
             fillOpacity: 0.6,
             color: 'white',
             weight: 0.5,
             opacity: 1
           });
           layer.on('click', function(e) {
             insertPopup(e.latlng, 'Vozidel zaparkovaných v placené zóně neoprávněně během měsíce ' + mesice[month] + ': ' + e.target.feature.properties['cnt_' + month])
           })
         } else {
           topoLayer.removeLayer(layer)
         };
       });
       topoLayer.addTo(map);
     }
   };

  drawMap(1);
})();