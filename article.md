---
title: "Nové modré zóny: stovky pokut rozdává auto s kamerami"
perex: "Koncem srpna zavedly městské části Praha 5, 6 a 8 nové zóny placeného stání. Obratem začalo město špatně parkujícím řidičům rozesílat složenky. Český rozhlas získal detailní data o tom, kde a kdy padlo nejvíc sankcí. Prohlédněte si interaktivní mapu."
description: "Koncem srpna zavedly městské části Praha 5, 6 a 8 nové zóny placeného stání. Obratem začalo město špatně parkujícím řidičům rozesílat složenky. Český rozhlas získal detailní data o tom, kde a kdy padlo nejvíc sankcí. Prohlédněte si interaktivní mapu."
authors: ["Jan Cibulka"]
published: "12. ledna 2017"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "modre-zony-v-praze"
libraries: [jquery, "https://unpkg.com/leaflet@1.0.2/dist/leaflet-src.js", "https://unpkg.com/topojson@2.2.0", "https://unpkg.com/esri-leaflet@2.0.6", "https://interaktivni.rozhlas.cz/tools/highcharts/5.0.6.min.js", "https://interaktivni.rozhlas.cz/tools/highcharts/data_5.0.6.js", "https://interaktivni.rozhlas.cz/tools/highcharts/heatmap_5.0.6.js", "https://cdnjs.cloudflare.com/ajax/libs/chroma-js/1.2.1/chroma.min.js", "https://unpkg.com/esri-leaflet-geocoder@2.2.2", "./js/_geostats.min.js"]
styles: ["https://unpkg.com/leaflet@1.0.2/dist/leaflet.css", "https://unpkg.com/esri-leaflet-geocoder@2.2.2/dist/esri-leaflet-geocoder.css", "./styl/local.css"]
socialimg: https://interaktivni.rozhlas.cz/modre-zony-v-praze/media/socimg.png
recommended:
  - link: http://www.rozhlas.cz/zpravy/regiony/_zprava/1643817
    title: V Praze 5 a 6 začaly fungovat parkovací zóny. Středočeskému kraji se to nelíbí
    perex: Kontroly budou provádět čtyři speciální vozy s kamerami, které budou snímat značky aut.
    image: https://interaktivni.rozhlas.cz/modre-zony-v-praze/media/modrezony.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/1604897
    title: Čištění pražských ulic provázejí stovky odtahů. Podívejte se, kdy a proč se odtahují auta u vás
    perex: Z území hlavního města odvezla správa služeb na odtahová parkoviště 29 010 automobilů, většinu z modrých parkovacích zón.
    image: https://interaktivni.rozhlas.cz/modre-zony-v-praze/media/prahamap.png
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/1496396
    title: Brno: Metropole odtahů. Řidiči za ně loni zaplatili přes 30 milionů
    perex: V Brně vznikl systém, který potřebuje velké množství odtažených aut. Řidiče stojí desítky milionů, městu jde přitom jen malá část. Problematický je vysoký počet blokových čištění.
    image: https://interaktivni.rozhlas.cz/modre-zony-v-praze/media/brno.jpg
---

Rozšíření parkovacích zón vyneslo magistrátní pokladně na pokutách už více než milion korun. Městská policie eviduje v nových zónách v Praze 6 měsíčně pět set přestupků spojených s parkováním. V Praze 5 jich loni v říjnu řešila skoro šest set. V méně exponované Praze 8, která  modré zóny zavedla jen na menší části svého území, strážníci ve stejném měsíci odhalili necelé dvě stovky přestupků spojených s parkovacími zónami. Vyplývá to z databáze městské policie, kterou Český rozhlas získal podle zákona o svobodném přístupu k informacím.

"Na základě výzvy správního orgánu bylo uhrazeno 2170 výzev, kde celková výše připsaných určených částek činí 1 082 700 Kč," odpověděl ředitel odboru dopravněsprávních činností pražského magistrátu Josef Mihalík na otázku, jaký finanční efekt rozšíření modrých zón hlavnímu městu přineslo. Jde o čísla za dobu od zavedení nových zón do současnosti.

V celé Praze řeší strážníci každý měsíc přes sedm tisíc parkovacích prohřešků, nejčastěji v centru města, tedy v Praze 1, 2 a 3.

<aside class="small">
  <h3>
    Jak funguje kamerové auto? Za hodinu zkontroluje přes dva tisíce zaparkovaných vozidel 
  </h3>
  <figure>
    <img src="https://interaktivni.rozhlas.cz/modre-zony-v-praze/media/auto.jpg" width="300">
    <audio src="https://samizdat.blob.core.windows.net/zvuky/kamery-auto.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Poslechněte si reportáž Filipa Titlbacha
  </figcaption>
</aside>

Drtivou většinu přestupků zaznamenalo kamerové auto společnosti Eltodo, která se o kontrolu parkovacích míst stará. Kamerový systém na vozidle kontroluje poznávací značky zaparkovaných aut a porovnává je s databází parkovacích povolení. Pokud počítač zjistí porušení pravidel, automaticky zašle městské policii fotografie a další informace o možném přestupku.

Detailně fungování kontrolních vozidel [popsal reportér Českého rozhlasu](http://www.rozhlas.cz/zpravy/regiony/_zprava/1635876). Kvůli automatizaci se na místě blokovou pokutou vyřešil pouze zlomek případů, většinu městská policie předala městu jako takzvané „oznámení o přestupku“.

Časové rozložení přestupků při parkování na vyhrazených místech si můžete prohlédnout v následujícím grafu, který obsahuje všech 82 tisíc případů od začátku roku 2016 do 20. listopadu téhož roku. Nejvíc přestupků hlásí centrum města, Praha 2 eviduje měsíčně skoro 4 tisíce špatně zaparkovaných vozů, Praha 1 ke třem tisícům.

<aside class="big">
  <div id="container" style="height: 320px;"></div>
</aside>

Naděje některých řidičů, že se zavedením nových zón nebude policie kontrolu stíhat, se nenaplnily. I přes stovky případů, které od září kvůli novým zónám přibyly, se v těch starých v centru žádný úbytek nekoná. Špatné parkování řeší pražská městská policie ve třech vlnách během dne, poprvé kolem deváté ranní, kdy lidé dorazí do práce, pak mezi jednou a čtvrtou hodinou odpolední, kdy se z práce vracejí. Třetí, o něco menší vrchol pak nastupuje mezi sedmou a devátou večerní - jde pravděpodobně o neukázněné řidiče jedoucí za zábavou.

<div data-bso=1></div>

Podobný trend vykazuje i Praha 5. Oproti tomu se šestá městská část s neoprávněně zaparkovanými vozidly potýká zejména dopoledne. Pravděpodobně jde o řidiče, kteří zde odstaví auto při cestě za prací.

Obecně je v Praze nejtěžší zaparkovat v centru okolo Národní třídy, Václavského či Karlova náměstí a u Náměstí Míru. Na druhém břehu řeky pak policisté rozdávají pokuty zejména okolo Letné, v blízkosti Veletržního paláce a u Ortenova náměstí. Riziková místa ukazuje následující mapa. Pomocí přepínače v levém horním rohu si můžete zobrazovat počty parkovacích přestupků podle jednotlivých měsíců loňského roku, modře jsou na mapě vyznačeny nově zavedené parkovací zóny.

<aside class="big">
<div id="mapdiv">
  <div id="geocode"></div>
  <div id="map"></div>
</div>
</aside>